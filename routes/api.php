<?php

use App\Http\Controllers\ApplicationInstallationController;
use App\Http\Controllers\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplicationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/applications', [ApplicationController::class, 'index'])->middleware('authenticated');
Route::post('/applications', [ApplicationController::class, 'store'])->middleware('authenticated');
Route::delete('/applications/{id}', [ApplicationController::class, 'destroy'])
    ->middleware('authenticated');
Route::get('/applications/{id}', [ApplicationController::class, 'show'])->middleware('authenticated');
Route::post('/installations', [ApplicationInstallationController::class, 'store'])
    ->middleware('authenticated');
Route::delete('/installations/{id}', [ApplicationInstallationController::class, 'destroy'])
    ->middleware('authenticated');
Route::get('payments', [PaymentController::class, 'index'])->middleware('authenticated');
