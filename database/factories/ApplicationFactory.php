<?php

namespace Database\Factories;

use App\Models\Application;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ApplicationFactory extends Factory {
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $isPaid = rand(0, 1);
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->text(200),
            'user_id' => User::whereHas('role', function($q){
                $q->where('slug', 'developer');
            })->first()->id,
            'amount' => $isPaid ? rand(1, 500) : 0,
            'icon_path' => env('APP_URL') . '/assets/images/' . $this->faker->image('public/assets/images', 400, 300, null, false)
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Application $application) {

        });
    }
}
