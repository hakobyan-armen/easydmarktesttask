<?php

namespace Database\Seeders;

use App\Models\Application;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            CompanyTableSeeder::class,
        ]);
        \App\Models\User::factory(3)->create();
        \App\Models\Application::factory(50)->create();
        $this->call([
            ApplicationInstallationSeeder::class,
        ]);
    }
}
