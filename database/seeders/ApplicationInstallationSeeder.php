<?php

namespace Database\Seeders;

use App\Models\Application;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class ApplicationInstallationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::whereHas('role', function($q){
            $q->where('slug', 'user');
        })->first();
        $applications = Application::whereDoesntHave('users', function ($q) use ($user){
            $q->where('user_id', $user->id);
        })->inRandomOrder()->limit(10)->get();
        foreach ($applications as $application){
            $user->installedApplications()->attach($application);
        }
    }
}
