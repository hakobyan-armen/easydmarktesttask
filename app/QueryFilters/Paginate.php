<?php

namespace App\QueryFilters;

use Closure;

class Paginate {

    const DEFAULT_PAGINATION_COUNT = 10;

    public function handle($request, Closure $next){
        $builder = $next($request);
        if(request()->has('page')){
            $builder->offset(request()->input('page') * self::DEFAULT_PAGINATION_COUNT);
        }
        $builder->limit(self::DEFAULT_PAGINATION_COUNT);
        return $builder;
    }
}
