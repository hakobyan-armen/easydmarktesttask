<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    public function users(){
        return $this->belongsToMany(User::class, 'application_user')->withTimestamps();
    }

    public function payments(){
        return $this->hasMany(Payment::class);
    }
}
