<?php

namespace App\Http\Controllers;

use App\Contracts\UserServiceContract;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $authUser;

    public function __construct(UserServiceContract $userService){
        $apiToken = request()->header('Authorization', '');
        $this->authUser = $userService->getAuthUser($apiToken);
        Config::set(['authUser' => $this->authUser]);
    }

    public function apiResponse($data, $statusCode = 200){
        if($statusCode === 204){
            return response()->noContent();
        }
        return response()->json($data, $statusCode);
    }

    protected function adjustResponse($response){
        if(isset($response['status'])){
            $statusCode = $response['status'];
            unset($response['status']);
        } else {
            $statusCode = 200;
        }
        return $this->apiResponse($response, $statusCode);

    }
}
