<?php

namespace App\Http\Controllers;

use App\Contracts\PaymentServiceContract;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    //

    public function index(PaymentServiceContract $paymentService){
        return $this->adjustResponse($paymentService->getSum($this->authUser));
    }
}
