<?php

namespace App\Http\Controllers;

use App\Contracts\ApplicationServiceContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ApplicationController extends Controller
{
    public function index(ApplicationServiceContract $applicationService): JsonResponse{
        return $this->apiResponse($applicationService->get($this->authUser), 200);
    }

    public function store(Request $request, ApplicationServiceContract $applicationService): JsonResponse{
        $response = $applicationService->store($request, $this->authUser);
        return $this->adjustResponse($response);
    }

    public function show($id, ApplicationServiceContract $applicationService): JsonResponse{
        return $this->adjustResponse($applicationService->find($id, $this->authUser));
    }

    public function destroy($id, ApplicationServiceContract $applicationService): Response{
        $response = $applicationService->destroy($id, $this->authUser);
        return $this->adjustResponse($response);
    }
}
