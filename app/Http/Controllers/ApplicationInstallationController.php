<?php

namespace App\Http\Controllers;

use App\Contracts\InstallationServiceContract;
use Illuminate\Http\Request;

class ApplicationInstallationController extends Controller
{
    //

    public function store(Request $request, InstallationServiceContract $installationService){
        return $this->adjustResponse($installationService->store($request, $this->authUser));
    }

    public function destroy($id, InstallationServiceContract $installationService){
        return $this->adjustResponse($installationService->destroy($id, $this->authUser));
    }
}
