<?php

namespace App\Repositories;

use App\Contracts\UserRepositoryContract;
use App\Models\User;

class UserRepository implements UserRepositoryContract{

    public function find(int $id): ?User{
        return User::find($id);
    }

    public function findByApiToken(string $apiToken): ?User{
        return User::where('api_token', $apiToken)->first();
    }
}
