<?php

namespace App\Repositories;

use App\Contracts\PaymentRepositoryContract;
use App\Models\Application;
use App\Models\Payment;

class PaymentRepository implements PaymentRepositoryContract {
    public function create($application, $user){
        $payment = new Payment();
        $payment->user_id = $user->id;
        $payment->application_id = $application->id;
        $payment->amount = $application->amount;
        $payment->save();
        return $payment;
    }

    public function findApplicationPayment($applicationId, $userId){
        return Payment::where('application_id', $applicationId)->where('user_id', $userId)->first();
    }

    public function findPaymentSum($user)
    {
        return $user->payments()->sum('amount');
    }

    public function findApplicationsPaymentsSum($user){
        return Application::where('applications.user_id', $user->id)
            ->join('payments', 'payments.application_id', '=', 'applications.id')
            ->sum('payments.amount');
    }

    public function findAllPaymentSum(){
        return Payment::sum('amount');
    }

}
