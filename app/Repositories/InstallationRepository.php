<?php

namespace App\Repositories;

use App\Contracts\InstallationRepositoryContract;

class InstallationRepository implements InstallationRepositoryContract {
    public function create($applicationId, $user){
        $user->installedApplications()->attach($applicationId);
    }

    public function destroy($id, $user){
        $user->installedApplications()->detach($id);
    }
}
