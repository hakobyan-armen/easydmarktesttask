<?php

namespace App\Repositories;

use App\Contracts\ApplicationRepositoryContract;
use App\Models\Application;
use App\QueryFilters\Paginate;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\DB;

class ApplicationRepository implements ApplicationRepositoryContract {

    public function all($user)
    {
        if($user->role->slug === 'developer'){
            $applicationQuery = $user->applications();
        }  else {
            $applicationQuery = Application::query();
            if($user->role->slug === 'user'){
                $applicationQuery->withCount(['users as is_installed' => function($q) use ($user){
                    $q->where('user_id', $user->id);
                }])->orderBy('is_installed', "DESC");
            }
        }
        return app(Pipeline::class)
            ->send($applicationQuery)
            ->through([
                Paginate::class,
            ])
            ->thenReturn()
            ->get();
    }

    public function create($request, $user): bool{
        $application = new Application();
        $application->name = $request->input('name');
        $application->description = $request->input('description');
        $application->icon_path = $request->input('icon_url');
        $application->amount = $request->input('amount');
        $application->user_id = $user->id;
        return $application->save();
    }

    public function find($id): ?Application{
        return Application::find($id);
    }

    public function destroy(Application $application){
        DB::table('application_user')->where('application_id', $application->id)->delete();
        return $application->delete();
    }
}
