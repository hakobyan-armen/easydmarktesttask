<?php

namespace App\Services;

use App\Contracts\PaymentRepositoryContract;
use App\Contracts\PaymentServiceContract;
use App\Models\Payment;

class PaymentService implements PaymentServiceContract {

    private $paymentRepository;
    const STORE_PERCENT = 40;

    public function __construct(PaymentRepositoryContract $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function getSum($user)
    {
        if($user->role->slug === 'developer'){
            $amount = floatval($this->paymentRepository->findApplicationsPaymentsSum($user))
                * (100 - self::STORE_PERCENT) / 100;
        } elseif($user->role->slug === 'admin'){
            $amount = floatval($this->paymentRepository->findAllPaymentSum()) * self::STORE_PERCENT / 100;
        } else {
            $amount = $this->paymentRepository->findPaymentSum($user);
        }
        return ['data' => $amount, 'status' => 200];
    }
}
