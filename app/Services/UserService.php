<?php

namespace App\Services;

use App\Contracts\UserRepositoryContract;
use App\Contracts\UserServiceContract;
use App\Models\User;

class UserService implements UserServiceContract
{

    private $userRepository;

    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAuthUser($token = ''): ?User{
        return $this->userRepository->findByApiToken($token);
    }
}
