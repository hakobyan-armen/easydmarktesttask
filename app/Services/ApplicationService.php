<?php

namespace App\Services;

use App\Contracts\ApplicationRepositoryContract;
use App\Contracts\ApplicationServiceContract;
use App\Models\Application;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class ApplicationService implements ApplicationServiceContract {

    private $applicationRepository;

    public function __construct(ApplicationRepositoryContract $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    public function get(User $user) {
        return $this->applicationRepository->all($user)->map(function ($item){
            $filteredData = [
                'id' => $item->id,
                'name' => $item->name,
                'description' => $item->description,
                'created_at' => $item->created_at
            ];
            if(isset($item->is_installed)){
                $filteredData['is_installed'] = $item->is_installed;
            }
            return $filteredData;
        });
    }

    public function find($id, $user){
        if(Gate::forUser($user)->denies('view-application')){
            return ['message' => 'You don\'t have permission for this action', 'status' => 403];
        }
        $application = $this->applicationRepository->find($id);
        if($application){
            return $application;
        }
        return ['message' => 'Application with given id does not exists', 'status' => 404];
    }

    public function store(Request $request, $authUser){
        if(Gate::forUser($authUser)->denies('add-application')){
            return ['message' => 'You don\'t have permission for this action', 'status' => 403];
        }
        $validation = Validator::make($request->all(), [
            'name' => ['required', 'max:255'],
            'description' => ['required'],
            'icon_url' => ['required', 'url', 'max:255'],
            'amount' => ['required', 'numeric'],
        ]);
        if($validation->fails()){
            return ['message' => $validation->errors(), 'status' => 400];
        }
        try {
            $this->applicationRepository->create($request, $authUser);
            return ['message' => 'Application is created successfully', 'status' => 201];
        } catch (\Exception $exception){
            return ['message' => $exception->getMessage(), 'status' => 500];
        }
    }

    public function destroy($id, $user){
        $application = $this->applicationRepository->find($id);
        if(!$application){
            return ['message' => 'Application with given id does not exists', 'status' => 404];
        }
        if(Gate::forUser($user)->denies('delete-application', $application)){
            return ['message' =>  'You don\'t have permission for this action', 'status' => 403];
        }
        try {
            $this->applicationRepository->destroy($application);
            return ['message' => 'noContent', 'status' => 204];
        } catch ( \Exception $exception){
            return ['message' => $exception->getMessage(), 'status' => 500];
        }
    }
}
