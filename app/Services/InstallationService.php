<?php

namespace App\Services;

use App\Contracts\ApplicationRepositoryContract;
use App\Contracts\ApplicationServiceContract;
use App\Contracts\InstallationRepositoryContract;
use App\Contracts\InstallationServiceContract;
use App\Contracts\PaymentRepositoryContract;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class InstallationService implements InstallationServiceContract {

    private $installationRepository;
    private $applicationRepository;
    private $paymentRepository;

    public function __construct(InstallationRepositoryContract $installationRepository,
                                ApplicationRepositoryContract $applicationRepository,
                                PaymentRepositoryContract $paymentRepository
    )
    {
        $this->installationRepository = $installationRepository;
        $this->applicationRepository = $applicationRepository;
        $this->paymentRepository = $paymentRepository;
    }

    public function store($request, $authUser)
    {
        if(Gate::forUser($authUser)->denies('install-application')){
            return ['message' => 'You don\'t have permission for this action', 'status' => 403];
        }
        $validation = Validator::make($request->all(), [
            'application_id' => ['required', 'exists:applications,id'],
        ]);
        if($validation->fails()){
            return ['message' => $validation->errors(), 'status' => 400];
        }
        if($authUser->installedApplications()->where('application_id', $request->input('application_id'))->exists()){
            return ['message' => 'Application already installed', 'status' => 400];
        }
        try {
            $application = $this->applicationRepository->find($request->input('application_id'));
            if(floatval($application->amount) > 0){
                $applicationPayment = $this->paymentRepository->findApplicationPayment($application->id, $authUser->id);
                if(!$applicationPayment){
                    $this->paymentRepository->create($application, $authUser);
                }
            }
            $this->installationRepository->create($application->id, $authUser);
            return ['message' => 'Application successfully installed', 'status' => 201];
        } catch (\Exception $exception){
            return ['message' => $exception->getMessage(), 'status' => 500];
        }

    }

    public function destroy($id, $authUser)
    {
        try {
            $application = $this->applicationRepository->find($id);
            if(!$application){
                return ['message' => 'Application with given id does not exists', 'status' => 404];
            }
            if(!$authUser->installedApplications()->where('application_id', $id)->exists()){
                return ['message' => 'Application does not installed', 400];
            }
            $this->installationRepository->destroy($id, $authUser);
            return ['message' => '', 'status' => 204];
        } catch (\Exception $exception){
            return ['message' => $exception->getMessage(), 'status' => 500];
        }

    }
}
