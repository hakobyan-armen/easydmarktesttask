<?php

namespace App\Contracts;

use App\Models\Application;

interface ApplicationRepositoryContract {

    public function all($user);

    public function create($request, $user);

    public function find($id): ?Application;

    public function destroy(Application $application);

}
