<?php

namespace App\Contracts;

use App\Models\User;
use Illuminate\Http\Request;

interface ApplicationServiceContract {

    public function get(User $user);

    public function store(Request $request, $authUser);
}
