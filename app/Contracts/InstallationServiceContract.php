<?php

namespace App\Contracts;

interface InstallationServiceContract {

    public function store($request, $authUser);

    public function destroy($id, $authUser);
}
