<?php

namespace App\Contracts;

interface PaymentRepositoryContract {
    public function create($application, $user);

    public function findApplicationPayment($applicationId, $userId);

    public function findPaymentSum($user);

    public function findApplicationsPaymentsSum($user);

    public function findAllPaymentSum();
}
