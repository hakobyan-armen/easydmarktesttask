<?php

namespace App\Contracts;

use App\Models\User;

interface UserServiceContract
{
    public function getAuthUser($token = ''): ?User;
}
