<?php

namespace App\Contracts;

interface InstallationRepositoryContract {
    public function create($applicationId, $user);
}
