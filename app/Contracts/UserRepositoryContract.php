<?php

namespace App\Contracts;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

interface UserRepositoryContract {

    public function find(int $id): ?User;

    public function findByApiToken(string $apiToken): ?User;
}
