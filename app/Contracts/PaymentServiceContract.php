<?php

namespace App\Contracts;

interface PaymentServiceContract {
    public function getSum($user);
}
