<?php

namespace App\Providers;

use App\Models\Application;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function($user){
            if($user->role->slug === 'admin'){
                return true;
            }
        });

        Gate::define('view-applications', function(User $user){
            $userRole = $user->role;
            return $userRole->slug === 'developer' || $userRole->slug === "user";
        });

        Gate::define('view-application', function(User $user){
            $userRole = $user->role;
            return $userRole->slug === 'developer' || $userRole->slug === "user";
        });

        Gate::define('add-application', function(User $user){
            return $user->role->slug === 'developer';
        });
        Gate::define('install-application', function(User $user){
            return $user->role->slug === 'user';
        });

        Gate::define('delete-application', function(User $user, Application $application){
            return $application->user_id === $user->id;
        });
//        Gate::define('uninstall-application', function(User $user, Application $application){
//            return $application->user_id === $user->id;
//        });
        //
    }
}
