<?php

namespace App\Providers;

use App\Contracts\ApplicationRepositoryContract;
use App\Contracts\ApplicationServiceContract;
use App\Contracts\InstallationRepositoryContract;
use App\Contracts\InstallationServiceContract;
use App\Contracts\PaymentRepositoryContract;
use App\Contracts\PaymentServiceContract;
use App\Contracts\UserServiceContract;
use App\Repositories\ApplicationRepository;
use App\Repositories\InstallationRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\UserRepository;
use App\Services\ApplicationService;
use App\Services\InstallationService;
use App\Services\PaymentService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class BindingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when([ApplicationService::class, InstallationService::class])
            ->needs(ApplicationRepositoryContract::class)
            ->give(ApplicationRepository::class);
        $this->app->when(InstallationService::class)
            ->needs(PaymentRepositoryContract::class)
            ->give(PaymentRepository::class);
        $this->app->when(PaymentService::class)
            ->needs(PaymentRepositoryContract::class)
            ->give(PaymentRepository::class);
        $this->app->when(InstallationService::class)
            ->needs(InstallationRepositoryContract::class)
            ->give(InstallationRepository::class);

        $this->app->bind(UserServiceContract::class, function(){
            $userRepository = new UserRepository();
            return new UserService($userRepository);
        });
        $this->app->bind(ApplicationServiceContract::class, ApplicationService::class);
        $this->app->bind(InstallationServiceContract::class, InstallationService::class);
        $this->app->bind(PaymentServiceContract::class, PaymentService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
